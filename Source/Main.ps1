using namespace System.Collections.Generic
using namespace System.IO

[FortahMain]::run($args)

class FortahMain {

    static [void] run([List[string]] $pArgs) {
        [string] $configPath = ''
        if ($pArgs.Count -ge 1) {
            $configPath = $pArgs[0]
        }
        [FortahCompiler] $compiler = [FortahCompiler]::new($configPath)
        $compiler.run()
    }   

}

class FortahCompiler {

    hidden [string] $configPath = ''
    hidden [string] $path = ''
    hidden [FortahConfig] $config = $null
    hidden [List[string]] $ignorePatterns = [List[string]]::new()
    hidden [List[string]] $collectPatterns = [List[string]]::new()
    hidden [List[string]] $collected = [List[string]]::new()
    hidden [StreamWriter] $writer = $null
    hidden [boolean] $previousLineEmpty = $false

    FortahCompiler([string] $pConfigPath) {
        $this.configPath = $pConfigPath
        $this.path = ''
        $this.config = $null
        $this.initialiseIgnorePatterns()
        $this.initialiseCollectPatterns()   
    }

    hidden [void] initialiseIgnorePatterns() {
        $this.ignorePatterns.Add('using module')
    }

    hidden [void] initialiseCollectPatterns() {
        $this.collectPatterns.Add('using namespace')
    }

    [void] run() {
        $this.initialise()
        $this.collectFromInput()
        $this.initialiseWriting()
        $this.writeCollected()
        $this.writeInput()
        $this.finaliseWriting()
        $this.finalise()
    }

    hidden [void] initialise() {
        Write-Host 'Fortah - PowerShell Complier - 1.0.0 - Jakub Hojnacki <jakubhojnacki@gmail.com>'
        Write-Host '-------------------------------------------------------------------------------'

        if ($this.configPath.Length -gt 0) {
            $this.path = [Path]::GetDirectoryName($this.configPath)
            $this.config = [FortahConfig]::parse($this.configPath)
        } else {
            throw 'You have to pass configuration file path as a parameter'
        }

        Write-Host "Output: $($this.config.getOutput())"
    }

    hidden [void] collectFromInput() {
        [string] $inputItem = ''
        foreach ($inputItem in $this.config.getInput()) {
            [string] $inputPath = [Path]::Combine($this.path, $inputItem)
            [StreamReader] $reader = [StreamReader]::new($inputPath)
            while( -not ($reader.EndOfStream)) {
                [string] $line = $reader.ReadLine()
                if ([FortahToolkit]::matchesPatterns($line, $this.collectPatterns)) {
                    if ( -not ($this.collected.Contains($line))) {
                        $this.collected.Add($line)
                    }
                }
            }
            $reader.Close()
            $reader.Dispose()
        }
    }

    hidden [void] initialiseWriting() {
        [string] $outputPath = [Path]::Combine($this.path, $this.config.getOutput())
        $this.writer = [StreamWriter]::new($outputPath)
    }

    hidden [void] writeCollected() {
        [bool] $written = $false
        [string] $collectedItem = ''
        foreach ($collectedItem in $this.collected) {
            $this.writer.WriteLine($collectedItem)
            $written = $true
        }
        if ($written) {
            $this.writer.WriteLine()
            $this.previousLineEmpty = $true
        }
    }

    hidden [void] writeInput() {
        [string] $inputItem = ''
        foreach ($inputItem in $this.config.getInput()) {
            Write-Host "Input: $inputItem"
            [string] $inputPath = [Path]::Combine($this.path, $inputItem)
            [StreamReader] $reader = [StreamReader]::new($inputPath)
            while( -not ($reader.EndOfStream)) {
                [string] $line = $reader.ReadLine()
                if ($this.shouldLineBeWritten($line)) {
                    $this.writer.WriteLine($line)
                }
            }
            $reader.Close()
            $reader.Dispose()
            if ( -not ($this.previousLineEmpty)) {
                $this.writer.WriteLine()
                $this.previousLineEmpty = $true
            }
        }
    }

    hidden [bool] shouldLineBeWritten([string] $pLine) {
        [bool] $result = $true
        if ($result) {
            if (($pLine.Trim().Length -eq 0) -and ($this.previousLineEmpty)) {
                $result = $false
            }
        }
        if ($result) {
            if ([FortahToolkit]::matchesPatterns($pLine.Trim(), $this.collectPatterns)) {
                $result = $false
            }
        }
        if ($result) {
            if ([FortahToolkit]::matchesPatterns($pLine.Trim(), $this.ignorePatterns)) {
                $result = $false
            }
        }
        if ($result) {
            $this.previousLineEmpty = ($pLine.Trim().Length -eq 0)
        }
        return $result
    }

    hidden [void] finaliseWriting() {
        $this.writer.Close()
        $this.writer.Dispose()
        $this.writer = $null
    }

    hidden [void] finalise() {
        Write-Host "-------------------------------------------------------------------------------"
        Write-Host "Completed."
    }

}

class FortahConfig {

    hidden [List[string]] $input = [List[string]]::new()
    [List[string]] getInput() { return $this.input }
    
    hidden [string] $output = ''
    [string] getOutput() { return $this.output }
    [void] setOutput([string] $pValue) { $this.output = $pValue }

    FortahConfig() {
    }

    static [FortahConfig] parse([string] $pPath) {
        [FortahConfig] $config = [FortahConfig]::new()
        [Object] $raw = (Get-Content ($pPath) | Out-String | ConvertFrom-Json)
        [string] $inputItem = ''
        foreach ($inputItem in $raw.input) {
            $config.getInput().Add($inputItem)
        }
        $config.setOutput($raw.output)
        return $config
    }

}

class FortahToolkit {

    static [bool] matchesPatterns([string] $pString, [List[string]] $pPatterns) {
        [bool] $result = $false
        [string] $pattern = ''
        foreach ($pattern in $pPatterns) {
            if ($pString.StartsWith($pattern)) {
                $result = $true
                break
            }
        }
        return $result
    }

}